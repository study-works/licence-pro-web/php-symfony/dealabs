# Projet Dealabs de Gaëthan BRUGIÈRE - Licence Pro Web

## Installation
```
# Clone
git clone git@gitlab.iut-clermont.uca.fr:php-symfony/promo-2021/dealabs/brugiere.git
cd brugiere

# Changer les droits du dossier public/upload
chmod 777 public/uploads/

# Lancer docker
docker-compose up -d
```
- Créer le **.env.local** :
```
# Mailer
MAILER_DSN=smtp://172.17.0.1:1026 # Peut être différent en fonction de docker

# Paramètres de la base de données
DB_SERVER=172.20.0.5 # Peut être différent en fonction de docker
DB_PORT=3310
DB_NAME=dealabs
DB_VERSION=5.7

# Paramètres de l'utilisateur
DB_USER=dealabs
DB_PASS=dealabs
```
- Installation des dépendances PHP
```
# On se met dans le bash du docker de php pour avoir la bonne version de php
docker exec -it -u root dealabs_php bash

# Symfony
composer install

# php-cs-fixer
cd tools/php-cs-fixer
composer install

exit
```
Php-cs-fixer doit être configurer dans l'IDE afin d'utiliser le fichier `.php-cs-fixer.dist.php`. Les dépendances sont dans [./tools/php-cs-fixer](https://gitlab.iut-clermont.uca.fr/php-symfony/promo-2021/dealabs/brugiere/-/tree/master/tools/php-cs-fixer).

- Gestion de la base
```
# Création de la base
php bin/console doctrine:migrations:migrate
```
- Installation des dépendances CSS/JS
```
# Installation des dépendances
yarn install

# Compilation du code
yarn build
```
- /Facultatif\ Ajout de données fictives\
De nombreux utilisateurs sont ajoutés. La création peut donc mettre un peu de temps.\
Il se peut aussi qu'elle échoue à cause d'une clé dupliquée. Il suffit alors simplement de relancer la commande.\
Pour se connecter, le mot de passe correspond à l'adresse e-mail de l'utilisateur.
```
# Création de données
php bin/console doctrine:fixtures:load
```

## API
### Conf Postman
La conf Postman est le fichier [Dealabs-PhpSymfony-LproWeb.postman_collection.json](Dealabs-PhpSymfony-LproWeb.postman_collection.json).\
Pour la requête permettant de récupérer les deals sauvegardés par un utilisateur, il faut penser à modifier le header `X-AUTH-TOKEN` afin de mettre le token d'un utilisateur valide.
