import $ from 'jquery';

/**
 * Deal preview
 */
$(function () {
  /**
   * Management of the coupon
   */
  const $jsDealCoupon = $('.js-deal-coupon');
  $(document).on('input', '.js-deal-input-coupon', function () {
    $jsDealCoupon.html($(this).val());
    $jsDealCoupon.parent().removeClass('d-none');

    if (!$(this).val()) {
      $jsDealCoupon.parent().addClass('d-none');
    }
  });

  /**
   * Management of the title
   */
  $(document).on('input', '.js-deal-input-title', function () {
    $('.js-deal-title').html($(this).val());
  });

  /**
   * Management of the description
   */
  $(document).on('input', '.js-deal-input-description', function () {
    if ($(this).val().length < 75) {
      $('.js-deal-description').html($(this).val() + "...");
    }
  });

  /**
   * Management of the img
   */
  $(document).on('change', '.js-deal-input-img', function () {
    imagePreview(this);
  });
});

/**
 * Coupon preview
 */
$(function () {
  /**
   * Management of the reduction type
   */
  const jsDealInputType = ".js-deal-input-type";
  const jsDealInputReduction = ".js-deal-input-reduction";
  const $jsDealFreeDelivery = $('.js-deal-free-delivery');
  const $jsDealReductionType = $('.js-deal-reduction-type');
  const $jsDealReduction = $('.js-deal-reduction');
  const $dealInputReductionGroup = $(jsDealInputReduction).closest('.form-group');

  if ($(jsDealInputType).val() !== "0" && $(jsDealInputType).val() !== "1") {
    $dealInputReductionGroup.addClass('d-none');
  } else {
    $jsDealReduction.parent().removeClass('d-none');
  }

  $(document).on('change', jsDealInputType, function () {
    if ($(this).val() === "0") {
      $jsDealReductionType.html("%");
    } else if ($(this).val() === "1") {
      $jsDealReductionType.html("€");
    }

    if ($(this).val() === "0" || $(this).val() === "1") {
      $jsDealFreeDelivery.addClass('d-none');
      $jsDealReduction.parent().removeClass('d-none');
      $dealInputReductionGroup.removeClass('d-none');
    } else if ($(this).val() === "2") {
      $jsDealFreeDelivery.removeClass('d-none');
      $jsDealReduction.parent().addClass('d-none');
      $dealInputReductionGroup.addClass('d-none');

      $jsDealReduction.html(null);
      $(jsDealInputReduction).val(null);
    } else {
      $jsDealFreeDelivery.addClass('d-none');
      $jsDealReduction.parent().addClass('d-none');
      $dealInputReductionGroup.addClass('d-none');
    }
  });

  $(document).on('input', jsDealInputReduction, function () {
    $jsDealReduction.html($(this).val());
  });
});

/**
 * Good type preview
 */
$(function () {
  const $jsDealPrice = $('.js-deal-price');
  $(document).on('input', '.js-deal-input-price', function () {
    $jsDealPrice.html($(this).val());
    $jsDealPrice.parent().removeClass('d-none');

    if (!$(this).val()) {
      $jsDealPrice.parent().addClass('d-none');
    }
  });

  const $jsDealUsualPrice = $('.js-deal-usual-price');
  $(document).on('input', '.js-deal-input-usual-price', function () {
    $jsDealUsualPrice.html($(this).val());
    $jsDealUsualPrice.parent().removeClass('d-none');

    if (!$(this).val()) {
      $jsDealUsualPrice.parent().addClass('d-none');
    }
  });

  const $jsDealDeliveryCosts = $('.js-deal-delivery-costs');
  $(document).on('input', '.js-deal-input-delivery-costs', function () {
    $jsDealDeliveryCosts.html($(this).val());
    $jsDealDeliveryCosts.parent().removeClass('d-none');

    if (!$(this).val()) {
      $jsDealDeliveryCosts.parent().addClass('d-none');
      $('.js-deal-free-delivery').addClass('d-none');
    }
    if ($(this).val() === "0") {
      $('.js-deal-free-delivery').removeClass('d-none');
      $jsDealDeliveryCosts.parent().addClass('d-none');
    }
  });
});

/**
 * Preview the image in form
 */
function imagePreview(input) {
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = function (e) {
      $('.js-deal-img').attr("src", e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
