import $ from 'jquery';
import Routing from './_routing';

let $jsDealRate = $('.js-deal-rating');
let $jsFlashMessages = $('.js-flash-messages');

$(document).on('click', '.js-deal-rating-down', function () {
  let $deal = $(this).closest('.js-deal').data('deal');
  let $value = $(this).closest($jsDealRate).find('.js-deal-rating-value');

  $.ajax({
    method: "POST",
    url: Routing.generate('app_deal_rating_ajax'),
    data: { dealId: $deal, value: -1 }
  }).done(function (response) {
    if (response.done) {
      let value = parseInt($value.html());
      $value.text((--value).toString() + "°");
    } else {
      $jsFlashMessages.append(response.message);
    }
  });
});

$(document).on('click', '.js-deal-rating-up', function () {
  let $deal = $(this).closest('.js-deal').data('deal');
  let $value = $(this).closest($jsDealRate).find('.js-deal-rating-value');

  $.ajax({
    method: "POST",
    url: Routing.generate('app_deal_rating_ajax'),
    data: { dealId: $deal, value: +1 }
  }).done(function (response) {
    if (response.done) {
      let value = parseInt($value.html());
      $value.text((++value).toString() + "°");
    } else {
      $jsFlashMessages.append(response.message);
    }
  });
});

$(document).on('click', '.js-save-deal', function () {
  let $this = $(this);
  let $deal = $this.closest('.js-deal').data('deal');

  let todo = 'add';

  if ($this.hasClass('js-isSaved')) {
    todo = 'remove';
  }

  $.ajax({
    method: "POST",
    url: Routing.generate('app_deal_save_ajax'),
    data: { dealId: $deal, toDo: todo }
  }).done(function (response) {
    if (response.done) {
      $this.toggleClass('bi-bookmark');
      $this.toggleClass('bi-bookmark-fill');
    } else {
      $jsFlashMessages.append(response.message);
    }
  });
});

$(document).on('click', '.js-deal-set-expired', function (event) {
  event.preventDefault();
  let $deal = $(this).closest('.js-deal').data('deal');

  $.ajax({
    method: "POST",
    url: Routing.generate('app_deal_set_expired_ajax'),
    data: { dealId: $deal }
  }).done(function (response) {
    if (response.done) {
      window.location.href = response.route;
    }
  });
});
