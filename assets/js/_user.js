import $ from 'jquery';
import Routing from "./_routing";

let $jsFlashMessages = $('.js-flash-messages');

$(document).on('change', '.js-alert-send-mail', function () {
  let $userAlert = $(this).closest('.js-user-alert').data('user-alert');

  $.ajax({
    method: "POST",
    url: Routing.generate('app_alert_change_send_email_ajax'),
    data: { userAlertId: $userAlert, sendMail: $(this).prop('checked') }
  }).done(function (response) {
    if (!response.done) {
      $jsFlashMessages.append(response.message);
    }
  });
});

$(document).on('click', '.js-alert-delete', function (event) {
  event.preventDefault();

  let $jsUserAlert = $(this).closest('.js-user-alert');
  let $userAlert = $jsUserAlert.data('user-alert');

  $.ajax({
    method: "POST",
    url: Routing.generate('app_alert_delete_ajax'),
    data: { userAlertId: $userAlert }
  }).done(function (response) {
    if (response.done) {
      $jsUserAlert.remove();
    } else {
      $jsFlashMessages.append(response.message);
    }
  });
});
