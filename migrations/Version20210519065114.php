<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210519065114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, deal_id INT NOT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, is_visible TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_9474526CA76ED395 (user_id), INDEX IDX_9474526CF60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coupon (id INT NOT NULL, type INT DEFAULT NULL, reduction DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deal (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, link LONGTEXT DEFAULT NULL, coupon VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, is_visible TINYINT(1) DEFAULT \'1\' NOT NULL, is_expired TINYINT(1) DEFAULT \'0\' NOT NULL, dtype VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E3FEC116989D9B62 (slug), INDEX IDX_E3FEC116A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deal_group (deal_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_2B1F87D9F60E2305 (deal_id), INDEX IDX_2B1F87D9FE54D947 (group_id), PRIMARY KEY(deal_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deal_rating (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, deal_id INT NOT NULL, value INT NOT NULL, INDEX IDX_CCCEA5AEA76ED395 (user_id), INDEX IDX_CCCEA5AEF60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE good_tip (id INT NOT NULL, price DOUBLE PRECISION DEFAULT NULL, usual_price DOUBLE PRECISION DEFAULT NULL, delivery_costs DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, is_visible TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_6DC044C55E237E06 (name), UNIQUE INDEX UNIQ_6DC044C5989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D64986CC499D (pseudo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id)');
        $this->addSql('ALTER TABLE coupon ADD CONSTRAINT FK_64BF3F02BF396750 FOREIGN KEY (id) REFERENCES deal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC116A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE deal_group ADD CONSTRAINT FK_2B1F87D9F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE deal_group ADD CONSTRAINT FK_2B1F87D9FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE deal_rating ADD CONSTRAINT FK_CCCEA5AEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE deal_rating ADD CONSTRAINT FK_CCCEA5AEF60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id)');
        $this->addSql('ALTER TABLE good_tip ADD CONSTRAINT FK_734E586FBF396750 FOREIGN KEY (id) REFERENCES deal (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CF60E2305');
        $this->addSql('ALTER TABLE coupon DROP FOREIGN KEY FK_64BF3F02BF396750');
        $this->addSql('ALTER TABLE deal_group DROP FOREIGN KEY FK_2B1F87D9F60E2305');
        $this->addSql('ALTER TABLE deal_rating DROP FOREIGN KEY FK_CCCEA5AEF60E2305');
        $this->addSql('ALTER TABLE good_tip DROP FOREIGN KEY FK_734E586FBF396750');
        $this->addSql('ALTER TABLE deal_group DROP FOREIGN KEY FK_2B1F87D9FE54D947');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE deal DROP FOREIGN KEY FK_E3FEC116A76ED395');
        $this->addSql('ALTER TABLE deal_rating DROP FOREIGN KEY FK_CCCEA5AEA76ED395');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE coupon');
        $this->addSql('DROP TABLE deal');
        $this->addSql('DROP TABLE deal_group');
        $this->addSql('DROP TABLE deal_rating');
        $this->addSql('DROP TABLE good_tip');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE user');
    }
}
