<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210625071539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE seller (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, link LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deal ADD seller_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC1168DE820D9 FOREIGN KEY (seller_id) REFERENCES seller (id)');
        $this->addSql('CREATE INDEX IDX_E3FEC1168DE820D9 ON deal (seller_id)');
        $this->addSql('ALTER TABLE `group` CHANGE is_visible is_visible TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('CREATE TABLE deal_report (id INT AUTO_INCREMENT NOT NULL, deal_id INT NOT NULL, user_id INT DEFAULT NULL, is_treat TINYINT(1) DEFAULT \'0\' NOT NULL, message LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D068F408F60E2305 (deal_id), INDEX IDX_D068F408A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deal_report ADD CONSTRAINT FK_D068F408F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id)');
        $this->addSql('ALTER TABLE deal_report ADD CONSTRAINT FK_D068F408A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE TABLE saved_deal (user_id INT NOT NULL, deal_id INT NOT NULL, INDEX IDX_EDC13303A76ED395 (user_id), INDEX IDX_EDC13303F60E2305 (deal_id), PRIMARY KEY(user_id, deal_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE saved_deal ADD CONSTRAINT FK_EDC13303A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE saved_deal ADD CONSTRAINT FK_EDC13303F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE TABLE badge (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, value INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_badge (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, badge_id INT NOT NULL, value INT NOT NULL, is_win TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1C32B345A76ED395 (user_id), INDEX IDX_1C32B345F7A2C2FC (badge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_badge ADD CONSTRAINT FK_1C32B345A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_badge ADD CONSTRAINT FK_1C32B345F7A2C2FC FOREIGN KEY (badge_id) REFERENCES badge (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1C32B345A76ED395F7A2C2FC ON user_badge (user_id, badge_id)');
        $this->addSql('CREATE TABLE alert_result (id INT AUTO_INCREMENT NOT NULL, user_alert_id INT NOT NULL, deal_id INT NOT NULL, is_see TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_2458A014738EBB72 (user_alert_id), INDEX IDX_2458A014F60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_alert (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, keyword VARCHAR(255) NOT NULL, min_rating INT DEFAULT 0 NOT NULL, send_mail TINYINT(1) DEFAULT \'0\' NOT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_F53FBD99989D9B62 (slug), INDEX IDX_F53FBD99A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE alert_result ADD CONSTRAINT FK_2458A014738EBB72 FOREIGN KEY (user_alert_id) REFERENCES user_alert (id)');
        $this->addSql('ALTER TABLE alert_result ADD CONSTRAINT FK_2458A014F60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id)');
        $this->addSql('ALTER TABLE user_alert ADD CONSTRAINT FK_F53FBD99A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2458A014738EBB72F60E2305 ON alert_result (user_alert_id, deal_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FEF0481D5E237E06 ON badge (name)');
        $this->addSql('ALTER TABLE user ADD api_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497BA2F5EB ON user (api_token)');
        $this->addSql('ALTER TABLE deal ADD image_name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE deal DROP image_name');
        $this->addSql('DROP INDEX UNIQ_8D93D6497BA2F5EB ON user');
        $this->addSql('ALTER TABLE user DROP api_token');
        $this->addSql('ALTER TABLE alert_result DROP FOREIGN KEY FK_2458A014738EBB72');
        $this->addSql('DROP INDEX UNIQ_2458A014738EBB72F60E2305 ON alert_result');
        $this->addSql('DROP TABLE alert_result');
        $this->addSql('DROP TABLE user_alert');
        $this->addSql('DROP INDEX UNIQ_FEF0481D5E237E06 ON badge');
        $this->addSql('DROP INDEX UNIQ_1C32B345A76ED395F7A2C2FC ON user_badge');
        $this->addSql('ALTER TABLE user_badge DROP FOREIGN KEY FK_1C32B345F7A2C2FC');
        $this->addSql('DROP TABLE badge');
        $this->addSql('DROP TABLE user_badge');
        $this->addSql('ALTER TABLE user DROP deleted_at');
        $this->addSql('DROP TABLE saved_deal');
        $this->addSql('DROP TABLE deal_report');
        $this->addSql('ALTER TABLE deal DROP FOREIGN KEY FK_E3FEC1168DE820D9');
        $this->addSql('DROP TABLE seller');
        $this->addSql('DROP INDEX IDX_E3FEC1168DE820D9 ON deal');
        $this->addSql('ALTER TABLE deal DROP seller_id');
        $this->addSql('ALTER TABLE `group` CHANGE is_visible is_visible TINYINT(1) NOT NULL');
    }
}
