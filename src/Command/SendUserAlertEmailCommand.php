<?php

namespace App\Command;

use App\Entity\UserAlert;
use App\Manager\DealManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class SendUserAlertEmailCommand extends Command
{
    protected static $defaultName = 'send-user-alert-email';

    private $dealManager;
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, DealManager $dealManager, MailerInterface $mailer)
    {
        $this->dealManager   = $dealManager;
        $this->entityManager = $entityManager;
        $this->mailer        = $mailer;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Envoie un email avec les deals d\'une alerte utilisateur.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $userAlerts = $this->entityManager->getRepository(UserAlert::class)->getWithUserBy(['sendMail' => true]);
        $nbEmail    = 0;

        foreach ($userAlerts as $userAlert) {
            $alertResult = $this->dealManager->getDealsFromAlert($userAlert->getId(), true, true);

            if (!isset($alertResult)) {
                continue;
            }

            $deals         = $alertResult[0];
            $nbComments    = $alertResult[1];
            $ratings       = $alertResult[2];

            $email = new TemplatedEmail();
            $email->from('user-alert@mydealabs.com')
                ->to($userAlert->getUser()->getEmail())
                ->subject('Dealabs: Mon alerte, ' . $userAlert->getKeyword())
                ->textTemplate('email/text/user-alert-deals.txt.twig')
                ->context([
                    'userAlert'       => $userAlert,
                    'deals'           => $deals,
                    'nbComments'      => $nbComments,
                    'ratings'         => $ratings,
                ]) // @TODO: Le port n'est pas set pour l'URL du deal
            ;

            try {
                $this->mailer->send($email);
                ++$email;
            } catch (TransportExceptionInterface $tei) {
                $output->writeln('Un mail d\'alerte utilisateur n\'a pas envoyé : ' . $tei->getTraceAsString());
            }
        }

        $output->writeln($nbEmail . ' e-mails envoyés.');

        return 0;
    }
}
