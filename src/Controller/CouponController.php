<?php

namespace App\Controller;

use App\Entity\Coupon;
use App\Entity\User;
use App\Event\DealPublishedEvent;
use App\Form\Type\CouponType;
use App\Manager\CouponManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CouponController extends AbstractController
{
    private $couponManager;
    private $eventDispatcher;

    public function __construct(CouponManager $couponManager, EventDispatcherInterface $eventDispatcher)
    {
        $this->couponManager   = $couponManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/codes-promo.html", name="app_coupon_showAll", methods={"GET"})
     */
    public function displayAllCoupon(): Response
    {
        $completeCoupons = $this->couponManager->getAllWithDependencies();
        $coupons         = $completeCoupons[0];
        $nbComments      = $completeCoupons[1];
        $ratings         = $completeCoupons[2];

        $dealsOfTheDay = $this->couponManager->getLastDayByRating();

        return $this->render('coupon/list.html.twig', [
            'deals'         => $coupons,
            'nbComments'    => $nbComments,
            'ratings'       => $ratings,
            'dealsOfTheDay' => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/codes-promo/hot.html", name="app_coupon_showAllHot", methods={"GET"})
     */
    public function displayAllHotCoupon(): Response
    {
        $completeCoupons = $this->couponManager->getAllHotWithDependencies();
        $coupons         = $completeCoupons[0];
        $nbComments      = $completeCoupons[1];
        $ratings         = $completeCoupons[2];

        $dealsOfTheDay = $this->couponManager->getLastDayByRating();

        return $this->render('coupon/list.html.twig', [
            'deals'         => $coupons,
            'nbComments'    => $nbComments,
            'ratings'       => $ratings,
            'dealsOfTheDay' => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/code-promo/nouveau.html", name="app_coupon_create", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function newCouponAction(Request $request): Response
    {
        $coupon = new Coupon();

        return $this->processForm($request, $coupon);
    }

    private function processForm(Request $request, Coupon $coupon): Response
    {
        $form = $this->createForm(CouponType::class, $coupon);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $coupon Coupon */
            $coupon = $form->getData();

            /** @var User $user */
            $user = $this->getUser();

            $coupon->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($coupon);
            $entityManager->flush();

            $this->eventDispatcher->dispatch(new DealPublishedEvent($user, $coupon));

            return $this->redirectToRoute('app_home_of_the_day');
        }

        return $this->render('coupon/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
