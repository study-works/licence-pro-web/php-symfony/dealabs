<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\DealRating;
use App\Entity\DealReport;
use App\Entity\User;
use App\Event\CommentAddedEvent;
use App\Event\DealSeenEvent;
use App\Event\RatingAddedEvent;
use App\Form\Type\CommentType;
use App\Form\Type\ReportDealType;
use App\Manager\DealManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DealController extends AbstractController
{
    private $dealManager;
    private $eventDispatcher;
    private $logger;
    private $mailer;
    private $translator;

    public function __construct(DealManager $dealManager, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger, MailerInterface $mailer, TranslatorInterface $translator)
    {
        $this->dealManager     = $dealManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger          = $logger;
        $this->mailer          = $mailer;
        $this->translator      = $translator;
    }

    /**
     * @Route("/deal/{slug}.html", name="app_show_deal", methods={"GET", "POST"})
     */
    public function showDealAction(Request $request, string $slug): Response
    {
        /** @var User $user */
        $user   = $this->getUser();
        $userId = isset($user) ? $user->getId() : null;

        $completeDeal = $this->dealManager->findDealBySlug($slug, $userId);
        $deal         = $completeDeal[0];
        $rating       = $completeDeal['rating'];
        $userWhoSaved = $completeDeal['user'] ?? null;

        $dealsOfTheDay = $this->dealManager->getLastDayByRating();

        if (!isset($deal)) {
            throw new NotFoundHttpException();
        }

        $form = null;

        if ($this->isGranted('ROLE_USER')) {
            $comment = new Comment();
            $comment->setDeal($deal);

            $form = $this->processForm($request, $comment);

            if ($form->isSubmitted() && $form->isValid()) {
                return $this->redirectToRoute('app_show_deal', ['slug' => $deal->getSlug()]);
            }

            $form = $form->createView();
        }

        $this->eventDispatcher->dispatch(new DealSeenEvent($user, $deal));

        return $this->render('deal/detail.html.twig', [
            'deal'          => $deal,
            'rating'        => $rating,
            'userWhoSaved'  => $userWhoSaved,
            'dealsOfTheDay' => $dealsOfTheDay,
            'form'          => $form,
        ]);
    }

    /**
     * @Route("/search", name="app_deal_search", methods={"GET"})
     */
    public function searchResultAction(Request $request): Response
    {
        $completeDeals = $this->dealManager->searchDeal(trim($request->query->get('q')));
        $deals         = $completeDeals[0];
        $nbComments    = $completeDeals[1];
        $ratings       = $completeDeals[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('deal/search.html.twig', [
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/deal/report/{slug}.html", name="app_deal_report", methods={"GET", "POST"})
     */
    public function reportAction(Request $request, string $slug): Response
    {
        $deal = $this->dealManager->getDealBySlug($slug);

        $form = $this->createForm(ReportDealType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var array<User> $admins */
            $admins = $this->getDoctrine()->getRepository(User::class)
                ->getByRole('ROLE_ADMIN');

            $message = $form->get('message')->getData();
            /** @var User $user */
            $user = $this->getUser();

            $dealReport = new DealReport();
            $dealReport->setMessage($message)
                ->setDeal($deal)
                ->setUser($user)
            ;

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dealReport);
            $entityManager->flush();

            $email = new TemplatedEmail();
            $email->from('report@mydealabs.com')
                ->to('admin@mydealabs.com')
                ->subject($this->translator->trans('report.email.subjet'))
                ->textTemplate('email/text/report-deal.txt.twig')
                ->context([
                    'deal'    => $deal,
                    'message' => $message,
                ])
            ;

            foreach ($admins as $admin) {
                $email->addTo($admin->getEmail());
            }

            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $tei) {
                $this->logger->error('Un utilisateur a signalé un deal, mais le mail ne c\'est pas envoyé : ' . $tei->getTraceAsString());
            }

            $this->addFlash('success', 'report.flash.message');

            return $this->redirectToRoute('app_home_index');
        }

        return $this->render('deal/report.html.twig', [
            'deal' => $deal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ajax/deal/rating", name="app_deal_rating_ajax", methods={"POST"}, options={"expose"=true})
     */
    public function addRating(Request $request): Response
    {
        /** @var User $user */
        $user  = $this->getUser();
        if (!isset($user)) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'rating.add.need_connection',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $value = $request->get('value');
        if ('-1' != $value && '1' != $value) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'rating.add.bad_value',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $deal  = $this->dealManager->getDealById($request->get('dealId'));
        if ($deal->getIsExpired()) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'rating.add.is_expired',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $rating = new DealRating();
        $rating->setUser($user);
        $rating->setDeal($deal);
        $rating->setValue($value);

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rating);
            $entityManager->flush();

            $this->eventDispatcher->dispatch(new RatingAddedEvent($user, $deal));
        } catch (UniqueConstraintViolationException $ucve) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'rating.add.already',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        return new JsonResponse([
            'done'    => true,
            'content' => 'rating.add.ok',
        ]);
    }

    /**
     * @Route("/ajax/deal/save", name="app_deal_save_ajax", methods={"POST"}, options={"expose"=true})
     */
    public function save(Request $request): Response
    {
        /** @var User $user */
        $user  = $this->getUser();

        if (!isset($user)) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'deal.save.need_connection',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $deal = $this->dealManager->getDealById($request->get('dealId'));
        if ('remove' == $request->get('toDo')) {
            $user->removeSavedDeal($deal);
        } elseif ($deal->getIsExpired()) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'deal.save.is_expired',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        } else {
            $user->addSavedDeal($deal);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new JsonResponse([
            'done'    => true,
        ]);
    }

    /**
     * @Route("/ajax/deal/set-expired", name="app_deal_set_expired_ajax", methods={"POST"}, options={"expose"=true})
     */
    public function setExpiredAction(Request $request)
    {
        $deal = $this->dealManager->getDealById($request->get('dealId'));
        $deal->setIsExpired(true);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        $this->addFlash('success', 'deal.set_expired.done');

        return new JsonResponse([
            'done'    => true,
            'route'   => $this->generateUrl('app_home_of_the_day'),
        ]);
    }

    private function processForm(Request $request, Comment $comment): FormInterface
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $comment Comment */
            $comment = $form->getData();

            /** @var User $user */
            $user = $this->getUser();

            $comment->setUser($user);

            $this->eventDispatcher->dispatch(new CommentAddedEvent($user));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
        }

        return $form;
    }
}
