<?php

namespace App\Controller;

use App\Manager\DealManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $dealManager;

    public function __construct(DealManager $dealManager)
    {
        $this->dealManager = $dealManager;
    }

    /**
     * @Route("/", name="app_home_index", methods={"GET"})
     */
    public function homeAction(): Response
    {
        return $this->redirectToRoute('app_home_of_the_day');
    }

    /**
     * @Route("/index/a-la-une.html", name="app_home_of_the_day", methods={"GET"})
     */
    public function homeOfTheDayAction(): Response
    {
        $completeDeals = $this->dealManager->getLastWeekByNbComment();
        $deals         = $completeDeals[0];
        $nbComments    = $completeDeals[1];
        $ratings       = $completeDeals[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('home.html.twig', [
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/index/hot.html", name="app_home_hot", methods={"GET"})
     */
    public function homeHotAction(): Response
    {
        $completeDeals = $this->dealManager->getAllHotByDate();
        $deals         = $completeDeals[0];
        $nbComments    = $completeDeals[1];
        $ratings       = $completeDeals[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('home.html.twig', [
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }
}
