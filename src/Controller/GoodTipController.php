<?php

namespace App\Controller;

use App\Entity\GoodTip;
use App\Entity\User;
use App\Event\DealPublishedEvent;
use App\Form\Type\GoodTipType;
use App\Manager\GoodTipManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class GoodTipController extends AbstractController
{
    private $goodTipManager;
    private $eventDispatcher;

    public function __construct(GoodTipManager $goodTipManager, EventDispatcherInterface $eventDispatcher)
    {
        $this->goodTipManager   = $goodTipManager;
        $this->eventDispatcher  = $eventDispatcher;
    }

    /**
     * @Route("/bons-plans.html", name="app_goodTip_showAll", methods={"GET"})
     */
    public function displayAllGoodTip(): Response
    {
        $completeGoodTips = $this->goodTipManager->getAllWithDependencies();
        $goodTips         = $completeGoodTips[0];
        $nbComments       = $completeGoodTips[1];
        $ratings          = $completeGoodTips[2];

        $dealsOfTheDay   = $this->goodTipManager->getLastDayByRating();

        return $this->render('good-tip/list.html.twig', [
            'deals'           => $goodTips,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/bons-plans/hot.html", name="app_goodTip_showAllHot", methods={"GET"})
     */
    public function displayAllHotGoodTip(): Response
    {
        $completeGoodTips = $this->goodTipManager->getAllHotWithDependencies();
        $goodTips         = $completeGoodTips[0];
        $nbComments       = $completeGoodTips[1];
        $ratings          = $completeGoodTips[2];

        $dealsOfTheDay   = $this->goodTipManager->getLastDayByRating();

        return $this->render('good-tip/list.html.twig', [
            'deals'           => $goodTips,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/bon-plan/nouveau.html", name="app_good_type_create", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function newCouponAction(Request $request): Response
    {
        $goodTip = new GoodTip();

        return $this->processForm($request, $goodTip);
    }

    private function processForm(Request $request, GoodTip $goodTip): Response
    {
        $form = $this->createForm(GoodTipType::class, $goodTip);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $goodTip GoodTip */
            $goodTip = $form->getData();

            /** @var User $user */
            $user = $this->getUser();

            $goodTip->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($goodTip);
            $entityManager->flush();

            $this->eventDispatcher->dispatch(new DealPublishedEvent($user, $goodTip));

            return $this->redirectToRoute('app_home_of_the_day');
        }

        return $this->render('good-tip/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
