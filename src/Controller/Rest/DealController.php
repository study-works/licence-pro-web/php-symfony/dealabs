<?php

namespace App\Controller\Rest;

use App\Entity\Deal;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class DealController extends AbstractFOSRestController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Rest\Get("/deals/last-week", name="app_deals_last_week_get", methods={"GET"})
     */
    public function getLastWeekAction(): Response
    {
        $deals = $this->getDoctrine()->getRepository(Deal::class)
            ->getLast('-1 weeks');

        $data = $this->serializer->serialize($deals, 'json');

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
