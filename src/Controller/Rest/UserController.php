<?php

namespace App\Controller\Rest;

use App\Entity\Deal;
use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractFOSRestController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Rest\Get("/user/saved-deals", name="app_user_saved_deals_get", methods={"GET"})
     */
    public function savedDealsList(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $deals = $this->getDoctrine()->getRepository(Deal::class)
            ->getUserSavedDeals($user->getId());

        $data = $this->serializer->serialize($deals, 'json');

        $view = $this->view($data, Response::HTTP_OK);

        return $this->handleView($view);
    }
}
