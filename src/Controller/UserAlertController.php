<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserAlert;
use App\Form\Type\AlertType;
use App\Manager\DealManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserAlertController extends AbstractController
{
    private $dealManager;

    public function __construct(DealManager $dealManager)
    {
        $this->dealManager  = $dealManager;
    }

    /**
     * @Route("/compte/mes-alertes.html", name="app_userAlerts_manage", methods={"GET", "POST"})
     */
    public function alertManagementAction(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->processAlertForm($request, $user);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('app_userAlerts_manage');
        }

        $alerts          = $this->getDoctrine()->getRepository(UserAlert::class)->findBy(['user' => $user->getId()]);
        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/alerts.html.twig', [
            'form'            => $form->createView(),
            'alerts'          => $alerts,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/ajax/alert/change-send-email", name="app_alert_change_send_email_ajax", methods={"POST"}, options={"expose"=true})
     */
    public function alertChangeSendEmail(Request $request): Response
    {
        /** @var User $user */
        $user  = $this->getUser();

        if (!isset($user)) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'deal.save.need_connection',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $userAlert = $this->getDoctrine()->getRepository(UserAlert::class)->findOneBy(['id' => $request->get('userAlertId'), 'user' => $user->getId()]);

        $userAlert->setSendMail('true' === $request->get('sendMail'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new JsonResponse([
            'done'    => true,
        ]);
    }

    /**
     * @Route("/ajax/alert/delete", name="app_alert_delete_ajax", methods={"POST"}, options={"expose"=true})
     */
    public function alertDelete(Request $request): Response
    {
        /** @var User $user */
        $user  = $this->getUser();

        if (!isset($user)) {
            $message = $this->renderView('layout/flash-message.html.twig', [
                'label' => 'danger',
                'flash' => 'deal.save.need_connection',
            ]);

            return new JsonResponse([
                'done'    => false,
                'message' => $message,
            ]);
        }

        $userAlert = $this->getDoctrine()->getRepository(UserAlert::class)->findOneBy(['id' => $request->get('userAlertId'), 'user' => $user->getId()]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($userAlert);
        foreach ($userAlert->getAlertResults() as $alertResult) {
            $entityManager->remove($alertResult);
        }
        $entityManager->flush();

        return new JsonResponse([
            'done'    => true,
        ]);
    }

    /**
     * @Route("/compte/alerte/{slug}.html", name="app_alert_see", methods={"GET"})
     */
    public function seeAlertResult(string $slug): Response
    {
        $userAlert = $this->getDoctrine()->getRepository(UserAlert::class)->findOneBy(['slug' => $slug]);

        $alertResult   = $this->dealManager->getDealsFromAlert($userAlert->getId());
        $deals         = $alertResult[0];
        $nbComments    = $alertResult[1];
        $ratings       = $alertResult[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/alert-result.html.twig', [
            'userAlert'       => $userAlert,
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * Form to manage alert.
     */
    private function processAlertForm(Request $request, User $user): FormInterface
    {
        $userAlert = new UserAlert();

        $form = $this->createForm(AlertType::class, $userAlert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $userAlert UserAlert */
            $userAlert = $form->getData();

            $userAlert->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userAlert);
            $entityManager->flush();
        }

        return $form;
    }
}
