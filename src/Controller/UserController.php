<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Manager\DealManager;
use App\Manager\UserManager;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserController extends AbstractController
{
    private $dealManager;
    private $userManager;
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage, DealManager $dealManager, UserManager $userManager)
    {
        $this->dealManager  = $dealManager;
        $this->userManager  = $userManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/compte/informations.html", name="app_informations_show", methods={"GET", "POST"})
     */
    public function showInformationsAction(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->processUserForm($request, $user);
    }

    /**
     * @Route("/compte/supprimer-mon-compte.html", name="app_user_delete", methods={"GET"})
     */
    public function deleteUserAction(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $user->setPseudo('User' . $user->getId());
        $user->setEmail($user->getId());
        $user->setPassword($user->getId());
        $user->setDeletedAt(new DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        /*
         * If I redirect to the app_logout route, to logout the user, I can't display flash message.
         * So, I remove the token to logout the user.
         */
        $this->tokenStorage->setToken();

        $this->addFlash('warning', 'user.count.isdeleted');

        return $this->redirectToRoute('app_home_of_the_day');
    }

    /**
     * @Route("/compte/mes-deals-sauvegardes.html", name="app_savedDeals_show", methods={"GET"})
     */
    public function savedDealsList(): Response
    {
        $completeDeals = $this->userManager->getSavedDeals($this->getUser()->getId());
        $deals         = $completeDeals[0];
        $nbComments    = $completeDeals[1];
        $ratings       = $completeDeals[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/saved-deals.html.twig', [
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/compte/mes-deals.html", name="app_userDeals_show", methods={"GET"})
     */
    public function userDealsList(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $completeDeals = $this->dealManager->getUserDeals($this->getUser()->getId());
        $deals         = $completeDeals[0];
        $nbComments    = $completeDeals[1];
        $ratings       = $completeDeals[2];

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/deals.html.twig', [
            'deals'           => $deals,
            'nbComments'      => $nbComments,
            'ratings'         => $ratings,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/compte/statistiques.html", name="app_userStatistics_show", methods={"GET"})
     */
    public function showStatisticsAction(): Response
    {
        $statistics      = $this->userManager->getUserStatistics($this->getUser()->getId());
        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/statistics.html.twig', [
            'statistics'      => $statistics,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * @Route("/compte/badges.html", name="app_userBadges_show", methods={"GET"})
     */
    public function showBadgeAction(): Response
    {
        $userBadges = $this->userManager->getFullUserBadge($this->getUser()->getId());

        $dealsOfTheDay   = $this->dealManager->getLastDayByRating();

        return $this->render('user/badges.html.twig', [
            'userBadges'      => $userBadges,
            'dealsOfTheDay'   => $dealsOfTheDay,
        ]);
    }

    /**
     * Form of User informations.
     */
    private function processUserForm(Request $request, User $user): Response
    {
        $username = $user->getUsername();
        $form     = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $user User */
            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'user.informations.ok');

            return $this->redirectToRoute('app_informations_show');
        }

        $user->setEmail($username);

        return $this->render('user/informations.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
