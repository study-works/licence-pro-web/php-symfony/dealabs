<?php

namespace App\DataFixtures;

use App\Entity\Badge;
use Doctrine\Persistence\ObjectManager;

class BadgeFixtures extends BaseFixtures
{
    public const BADGES = [
        ['name' => 'Surveillant', 'description' => 'Vous devez voter pour 10 deals.'],
        ['name' => 'Cobaye', 'description' => 'Vous devez poster au moins 10 deals.'],
        ['name' => 'Rapport de stage', 'description' => 'Vous devez poster au moins 10 commentaires.'],
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Badge::class, sizeof(self::BADGES), function (Badge $badge, $count) {
            $newBadge = self::BADGES[$count];
            $badge->setName($newBadge['name']);
            $badge->setDescription($newBadge['description']);
            $badge->setValue(10);
        });

        $manager->flush();
    }
}
