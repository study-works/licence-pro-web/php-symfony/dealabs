<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Coupon;
use App\Entity\GoodTip;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends BaseFixtures implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 75, function (Comment $comment, $count) {
            if ($this->faker->boolean()) {
                /** @var Coupon $deal */
                $deal = $this->getReference(Coupon::class . '_' . $this->faker->numberBetween(0, CouponFixtures::NB_COUPON - 5));
            } else {
                /** @var GoodTip $deal */
                $deal = $this->getReference(GoodTip::class . '_' . $this->faker->numberBetween(0, GoodTipFixtures::NB_GOOD_TYPE - 5));
            }

            /** @var User $user */
            $user = $this->getReference(User::class . '_' . $this->faker->numberBetween(0, UserFixtures::NB_USER - 1));

            $comment->setUser($user)
                ->setDeal($deal)
                ->setComment($this->faker->paragraph);
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            CouponFixtures::class,
            GoodTipFixtures::class,
        ];
    }
}
