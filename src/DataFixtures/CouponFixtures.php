<?php

namespace App\DataFixtures;

use App\Entity\Coupon;
use App\Entity\Group;
use App\Entity\Seller;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CouponFixtures extends BaseFixtures implements DependentFixtureInterface
{
    public const NB_COUPON  = 10;
    private static $coupons = ['save', 'coupon', 'reduc', 'toto', 'code'];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Coupon::class, self::NB_COUPON, function (Coupon $coupon, $count) {
            /** @var User $user */
            $user = $this->getReference(User::class . '_' . $this->faker->numberBetween(0, UserFixtures::NB_USER - 1));

            $coupon->setTitle($this->faker->sentence)
                ->setDescription($this->faker->paragraph)
                ->setCreatedAt($this->faker->dateTimeThisMonth())
                ->setUser($user);

            if ($this->faker->boolean()) {
                $coupon->setLink($this->faker->url);
            }

            if ($this->faker->boolean()) {
                $coupon->setCoupon($this->faker->randomElement(self::$coupons));
            }

            if ($this->faker->boolean()) {
                $type = $this->faker->numberBetween(0, 2);
                $coupon->setType($type);

                if (2 !== $type) {
                    $coupon->setReduction($this->faker->randomFloat(null, -25, 0));
                }
            }

            if ($this->faker->boolean(45)) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(0, 3));
                $coupon->addGroup($group);
            }

            if ($this->faker->boolean()) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(4, 6));
                $coupon->addGroup($group);
            }

            if ($this->faker->boolean(65)) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(7, 9));
                $coupon->addGroup($group);
            }

            if ($this->faker->boolean()) {
                /** @var Seller $seller */
                $seller = $this->getReference(Seller::class . '_' . $this->faker->numberBetween(0, 4));
                $coupon->setSeller($seller);
            }
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            GroupFixtures::class,
        ];
    }
}
