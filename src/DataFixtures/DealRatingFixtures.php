<?php

namespace App\DataFixtures;

use App\Entity\Coupon;
use App\Entity\DealRating;
use App\Entity\GoodTip;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DealRatingFixtures extends BaseFixtures implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(DealRating::class, UserFixtures::NB_USER, function (DealRating $dealRating, $count) {
            if ($this->faker->boolean()) {
                /** @var Coupon $deal */
                $deal = $this->getReference(Coupon::class . '_' . $this->faker->numberBetween(0, CouponFixtures::NB_COUPON - 5));
            } else {
                /** @var GoodTip $deal */
                $deal = $this->getReference(GoodTip::class . '_' . $this->faker->numberBetween(0, GoodTipFixtures::NB_GOOD_TYPE - 5));
            }

            /** @var User $user */
            $user = $this->getReference(User::class . '_' . $count);

            $dealRating->setUser($user)
                ->setDeal($deal)
                ->setValue($this->faker->randomElement([-1, 1, 1]));
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            CouponFixtures::class,
            GoodTipFixtures::class,
        ];
    }
}
