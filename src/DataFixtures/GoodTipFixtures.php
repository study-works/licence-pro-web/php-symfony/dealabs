<?php

namespace App\DataFixtures;

use App\Entity\GoodTip;
use App\Entity\Group;
use App\Entity\Seller;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GoodTipFixtures extends BaseFixtures implements DependentFixtureInterface
{
    public const NB_GOOD_TYPE = 10;
    private static $coupons   = ['save', 'coupon', 'reduc', 'toto', 'code'];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(GoodTip::class, self::NB_GOOD_TYPE, function (GoodTip $goodTip, $count) {
            /** @var User $user */
            $user = $this->getReference(User::class . '_' . $this->faker->numberBetween(0, UserFixtures::NB_USER - 1));

            $goodTip->setTitle($this->faker->sentence)
                ->setDescription($this->faker->paragraph)
                ->setCreatedAt($this->faker->dateTimeThisMonth())
                ->setUser($user);

            if ($this->faker->boolean()) {
                $goodTip->setLink($this->faker->url);
            }

            if ($this->faker->boolean()) {
                $goodTip->setCoupon($this->faker->randomElement(self::$coupons));
            }

            if ($this->faker->boolean()) {
                $price = $this->faker->randomFloat(2, 5, 5000);
                $goodTip->setUsualPrice($price);
                $goodTip->setPrice($this->faker->randomFloat(2, 0, $price));
            } else {
                $goodTip->setDeliveryCosts($this->faker->randomFloat(2, 0, 10));
            }

            if ($this->faker->boolean(45)) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(0, 3));
                $goodTip->addGroup($group);
            }

            if ($this->faker->boolean()) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(4, 6));
                $goodTip->addGroup($group);
            }

            if ($this->faker->boolean(65)) {
                /** @var Group $group */
                $group = $this->getReference(Group::class . '_' . $this->faker->numberBetween(7, 9));
                $goodTip->addGroup($group);
            }

            if ($this->faker->boolean()) {
                /** @var Seller $seller */
                $seller = $this->getReference(Seller::class . '_' . $this->faker->numberBetween(0, 4));
                $goodTip->setSeller($seller);
            }
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
