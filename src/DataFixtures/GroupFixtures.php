<?php

namespace App\DataFixtures;

use App\Entity\Group;
use Doctrine\Persistence\ObjectManager;

class GroupFixtures extends BaseFixtures
{
    private const GROUPS = [
        'High-Tech', 'Épiceries & courses', 'Modes & accessoires', 'Voyages', 'Familles & enfants',
        'Auto-Moto', 'Gratuit', 'Forfaits mobiles et internet', 'Jardin & bricolage', 'Services',
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Group::class, sizeof(self::GROUPS), function (Group $group, $count) {
            $group->setName(self::GROUPS[$count]);
        });

        $manager->flush();
    }
}
