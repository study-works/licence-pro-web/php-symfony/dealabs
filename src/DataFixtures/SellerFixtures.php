<?php

namespace App\DataFixtures;

use App\Entity\Seller;
use Doctrine\Persistence\ObjectManager;

class SellerFixtures extends BaseFixtures
{
    private const SELLERS = [
        ['name' => 'Dell', 'link' => 'https://www.dell.com'],
        ['name' => 'Dafy', 'link' => 'https://www.dafy-moto.com'],
        ['name' => 'topachat.com', 'link' => 'https://www.topachat.com'],
        ['name' => 'Auchan', 'link' => null],
        ['name' => 'L\'eau vive', 'link' => 'https://www.eau-vive.com'],
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Seller::class, sizeof(self::SELLERS), function (Seller $seller, $count) {
            $newSeller = self::SELLERS[$count];
            $seller->setName($newSeller['name']);
            $seller->setLink($newSeller['link']);
        });

        $manager->flush();
    }
}
