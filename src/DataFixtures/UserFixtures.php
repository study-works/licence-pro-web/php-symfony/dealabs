<?php

namespace App\DataFixtures;

use App\Entity\Badge;
use App\Entity\User;
use App\Entity\UserBadge;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixtures implements DependentFixtureInterface
{
    public const NB_USER = 200;

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, self::NB_USER, function (User $user, $count) {
            for ($i = 0; $i < count(BadgeFixtures::BADGES); ++$i) {
                /** @var Badge $badge */
                $badge = $this->getReference(Badge::class . '_' . $i);
                $userBadge = new UserBadge();
                $userBadge->setUser($user);
                $userBadge->setBadge($badge);
                $userBadge->setValue(0);

                $user->addUserBadge($userBadge);
            }

            $email = $this->faker->email;

            $user->setPseudo($this->faker->userName)
                ->setEmail($email)
                ->setPassword($this->passwordEncoder->encodePassword($user, $email));

            if ($this->faker->boolean(5)) {
                $user->setRoles(['ROLE_ADMIN']);
            }
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            BadgeFixtures::class,
        ];
    }
}
