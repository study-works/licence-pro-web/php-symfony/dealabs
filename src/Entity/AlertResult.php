<?php

namespace App\Entity;

use App\Repository\AlertResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(columns={"user_alert_id", "deal_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass=AlertResultRepository::class)
 */
class AlertResult
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserAlert::class, inversedBy="alertResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userAlert;

    /**
     * @ORM\ManyToOne(targetEntity=Deal::class, inversedBy="alertResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $deal;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $isSee = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserAlert(): ?UserAlert
    {
        return $this->userAlert;
    }

    public function setUserAlert(?UserAlert $userAlert): self
    {
        $this->userAlert = $userAlert;

        return $this;
    }

    public function getDeal(): ?Deal
    {
        return $this->deal;
    }

    public function setDeal(?Deal $deal): self
    {
        $this->deal = $deal;

        return $this;
    }

    public function getIsSee(): ?bool
    {
        return $this->isSee;
    }

    public function setIsSee(bool $isSee): self
    {
        $this->isSee = $isSee;

        return $this;
    }
}
