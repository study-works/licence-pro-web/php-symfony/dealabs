<?php

namespace App\Entity;

use App\Repository\CouponRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CouponRepository::class)
 */
class Coupon extends Deal
{
    /**
     * @Assert\Range(min = 0, max = 2, notInRangeMessage="coupon.create.not_valid_type")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type;

    /**
     * @Assert\Negative(message="coupon.create.not_valid_reduction")
     * @ORM\Column(type="float", nullable=true)
     */
    private $reduction;

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getReduction(): ?float
    {
        return $this->reduction;
    }

    public function setReduction(?float $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }
}
