<?php

namespace App\Entity;

use App\Repository\DealRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=DealRepository::class)
 * @InheritanceType("JOINED")
 *
 * @Serializer\ExclusionPolicy("ALL")
 *
 * @Vich\Uploadable
 */
class Deal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Url(message="deal.create.bad_url")
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $link;

    /**
     * @Assert\Length(max= 255, maxMessage="deal.create.coupon_too_long")
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $coupon;

    /**
     * @Assert\NotBlank(message="deal.create.title_blank")
     * @Assert\Length(max= 255, maxMessage="deal.create.title_too_long")
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     */
    private $title;

    /**
     * @Assert\NotBlank(message="deal.create.description_blank")
     * @ORM\Column(type="text")
     *
     * @Serializer\Expose
     */
    private $description;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Serializer\Expose
     */
    private $slug;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="deals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=DealRating::class, mappedBy="deal")
     */
    private $dealRatings;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="deal")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, inversedBy="deals")
     *
     * @Serializer\Expose
     */
    private $groups;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $isVisible = true;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $isExpired = false;

    /**
     * @ORM\ManyToOne(targetEntity=Seller::class, inversedBy="deals")
     *
     * @Serializer\Expose
     */
    private $seller;

    /**
     * @ORM\OneToMany(targetEntity=DealReport::class, mappedBy="deal")
     */
    private $dealReports;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="savedDeals")
     */
    private $usersWhoSaved;

    /**
     * @ORM\OneToMany(targetEntity=AlertResult::class, mappedBy="deal")
     */
    private $alertResults;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="deal_image", fileNameProperty="imageName")
     */
    private $imageFile;

    public function __construct()
    {
        $this->dealRatings   = new ArrayCollection();
        $this->comments      = new ArrayCollection();
        $this->groups        = new ArrayCollection();
        $this->dealReports   = new ArrayCollection();
        $this->usersWhoSaved = new ArrayCollection();
        $this->alertResults  = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCoupon(): ?string
    {
        return $this->coupon;
    }

    public function setCoupon(string $coupon): self
    {
        $this->coupon = $coupon;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|DealRating[]
     */
    public function getDealRatings(): Collection
    {
        return $this->dealRatings;
    }

    public function addDealRating(DealRating $dealRating): self
    {
        if (!$this->dealRatings->contains($dealRating)) {
            $this->dealRatings[] = $dealRating;
            $dealRating->setDeal($this);
        }

        return $this;
    }

    public function removeDealRating(DealRating $dealRating): self
    {
        if ($this->dealRatings->removeElement($dealRating)) {
            // set the owning side to null (unless already changed)
            if ($dealRating->getDeal() === $this) {
                $dealRating->setDeal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setDeal($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getDeal() === $this) {
                $comment->setDeal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        $this->groups->removeElement($group);

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    public function getIsExpired(): ?bool
    {
        return $this->isExpired;
    }

    public function setIsExpired(bool $isExpired): self
    {
        $this->isExpired = $isExpired;

        return $this;
    }

    public function getSeller(): ?Seller
    {
        return $this->seller;
    }

    public function setSeller(?Seller $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * @return Collection|DealReport[]
     */
    public function getDealReports(): Collection
    {
        return $this->dealReports;
    }

    public function addDealReport(DealReport $dealReport): self
    {
        if (!$this->dealReports->contains($dealReport)) {
            $this->dealReports[] = $dealReport;
            $dealReport->setDeal($this);
        }

        return $this;
    }

    public function removeDealReport(DealReport $dealReport): self
    {
        if ($this->dealReports->removeElement($dealReport)) {
            // set the owning side to null (unless already changed)
            if ($dealReport->getDeal() === $this) {
                $dealReport->setDeal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersWhoSaved(): Collection
    {
        return $this->usersWhoSaved;
    }

    public function addUsersWhoSaved(User $usersWhoSaved): self
    {
        if (!$this->usersWhoSaved->contains($usersWhoSaved)) {
            $this->usersWhoSaved[] = $usersWhoSaved;
            $usersWhoSaved->addSavedDeal($this);
        }

        return $this;
    }

    public function removeUsersWhoSaved(User $usersWhoSaved): self
    {
        if ($this->usersWhoSaved->removeElement($usersWhoSaved)) {
            $usersWhoSaved->removeSavedDeal($this);
        }

        return $this;
    }

    /**
     * @return Collection|AlertResult[]
     */
    public function getAlertResults(): Collection
    {
        return $this->alertResults;
    }

    public function addAlertResult(AlertResult $alertResult): self
    {
        if (!$this->alertResults->contains($alertResult)) {
            $this->alertResults[] = $alertResult;
            $alertResult->setDeal($this);
        }

        return $this;
    }

    public function removeAlertResult(AlertResult $alertResult): self
    {
        if ($this->alertResults->removeElement($alertResult)) {
            // set the owning side to null (unless already changed)
            if ($alertResult->getDeal() === $this) {
                $alertResult->setDeal(null);
            }
        }

        return $this;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }
}
