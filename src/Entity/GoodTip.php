<?php

namespace App\Entity;

use App\Repository\GoodTipRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=GoodTipRepository::class)
 */
class GoodTip extends Deal
{
    /**
     * @Assert\PositiveOrZero(message="good_tip.create.not_valid_price")
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @Assert\PositiveOrZero(message="good_tip.create.not_valid_usual_price")
     * @ORM\Column(type="float", nullable=true)
     */
    private $usualPrice;

    /**
     * @Assert\PositiveOrZero(message="good_tip.create.not_valid_delivery_costs")
     * @ORM\Column(type="float", nullable=true)
     */
    private $deliveryCosts;

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getUsualPrice(): ?float
    {
        return $this->usualPrice;
    }

    public function setUsualPrice(?float $usualPrice): self
    {
        $this->usualPrice = $usualPrice;

        return $this;
    }

    public function getDeliveryCosts(): ?float
    {
        return $this->deliveryCosts;
    }

    public function setDeliveryCosts(?float $deliveryCosts): self
    {
        $this->deliveryCosts = $deliveryCosts;

        return $this;
    }
}
