<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity(
 *     fields={"pseudo"},
 *     message="user.used_pseudo"
 * )
 * @UniqueEntity(
 *     fields={"email"},
 *     message="user.bad_email"
 * ) // The message is generic. This avoids indicating that an account already exists with this e-mail
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="user.blank_email")
     * @Assert\Email(mode="strict", message="user.bad_email")
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\NotBlank(message="signin.blank_password")
     * @Assert\Length(min = 8, minMessage = "signin.password_too_short", max="4096")
     *
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Assert\NotBlank(message="user.blank_pseudo")
     * @Assert\Length(max = 50, maxMessage = "user.pseudo_too_long")
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $pseudo;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Deal::class, mappedBy="user")
     */
    private $deals;

    /**
     * @ORM\OneToMany(targetEntity=DealRating::class, mappedBy="user")
     */
    private $dealRatings;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=DealReport::class, mappedBy="user")
     */
    private $dealReports;

    /**
     * @ORM\ManyToMany(targetEntity=Deal::class, inversedBy="usersWhoSaved")
     * @ORM\JoinTable(name="saved_deal")
     */
    private $savedDeals;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity=UserBadge::class, mappedBy="user", cascade={"persist"})
     */
    private $userBadges;

    /**
     * @ORM\OneToMany(targetEntity=UserAlert::class, mappedBy="user")
     */
    private $userAlerts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $apiToken;

    public function __construct()
    {
        $this->deals       = new ArrayCollection();
        $this->dealRatings = new ArrayCollection();
        $this->comments    = new ArrayCollection();
        $this->dealReports = new ArrayCollection();
        $this->savedDeals  = new ArrayCollection();
        $this->userBadges  = new ArrayCollection();
        $this->userAlerts  = new ArrayCollection();

        $this->apiToken = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getDeals(): Collection
    {
        return $this->deals;
    }

    public function addDeal(Deal $deal): self
    {
        if (!$this->deals->contains($deal)) {
            $this->deals[] = $deal;
            $deal->setUser($this);
        }

        return $this;
    }

    public function removeDeal(Deal $deal): self
    {
        if ($this->deals->removeElement($deal)) {
            // set the owning side to null (unless already changed)
            if ($deal->getUser() === $this) {
                $deal->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DealRating[]
     */
    public function getDealRatings(): Collection
    {
        return $this->dealRatings;
    }

    public function addDealRating(DealRating $dealRating): self
    {
        if (!$this->dealRatings->contains($dealRating)) {
            $this->dealRatings[] = $dealRating;
            $dealRating->setUser($this);
        }

        return $this;
    }

    public function removeDealRating(DealRating $dealRating): self
    {
        if ($this->dealRatings->removeElement($dealRating)) {
            // set the owning side to null (unless already changed)
            if ($dealRating->getUser() === $this) {
                $dealRating->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DealReport[]
     */
    public function getDealReports(): Collection
    {
        return $this->dealReports;
    }

    public function addDealReport(DealReport $dealReport): self
    {
        if (!$this->dealReports->contains($dealReport)) {
            $this->dealReports[] = $dealReport;
            $dealReport->setUser($this);
        }

        return $this;
    }

    public function removeDealReport(DealReport $dealReport): self
    {
        if ($this->dealReports->removeElement($dealReport)) {
            // set the owning side to null (unless already changed)
            if ($dealReport->getUser() === $this) {
                $dealReport->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deal[]
     */
    public function getSavedDeals(): Collection
    {
        return $this->savedDeals;
    }

    public function addSavedDeal(Deal $savedDeal): self
    {
        if (!$this->savedDeals->contains($savedDeal)) {
            $this->savedDeals[] = $savedDeal;
        }

        return $this;
    }

    public function removeSavedDeal(Deal $savedDeal): self
    {
        $this->savedDeals->removeElement($savedDeal);

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|UserBadge[]
     */
    public function getUserBadges(): Collection
    {
        return $this->userBadges;
    }

    public function addUserBadge(UserBadge $userBadge): self
    {
        if (!$this->userBadges->contains($userBadge)) {
            $this->userBadges[] = $userBadge;
            $userBadge->setUser($this);
        }

        return $this;
    }

    public function removeUserBadge(UserBadge $userBadge): self
    {
        if ($this->userBadges->removeElement($userBadge)) {
            // set the owning side to null (unless already changed)
            if ($userBadge->getUser() === $this) {
                $userBadge->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserAlert[]
     */
    public function getUserAlerts(): Collection
    {
        return $this->userAlerts;
    }

    public function addUserAlert(UserAlert $userAlert): self
    {
        if (!$this->userAlerts->contains($userAlert)) {
            $this->userAlerts[] = $userAlert;
            $userAlert->setUser($this);
        }

        return $this;
    }

    public function removeUserAlert(UserAlert $userAlert): self
    {
        if ($this->userAlerts->removeElement($userAlert)) {
            // set the owning side to null (unless already changed)
            if ($userAlert->getUser() === $this) {
                $userAlert->setUser(null);
            }
        }

        return $this;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(?string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }
}
