<?php

namespace App\Entity;

use App\Repository\UserAlertRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=UserAlertRepository::class)
 */
class UserAlert
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $keyword;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $minRating;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $sendMail = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userAlerts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=AlertResult::class, mappedBy="userAlert")
     */
    private $alertResults;

    /**
     * @Gedmo\Slug(fields={"keyword"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    public function __construct()
    {
        $this->alertResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getMinRating(): ?int
    {
        return $this->minRating;
    }

    public function setMinRating(int $minRating): self
    {
        $this->minRating = $minRating;

        return $this;
    }

    public function getSendMail(): ?bool
    {
        return $this->sendMail;
    }

    public function setSendMail(bool $sendMail): self
    {
        $this->sendMail = $sendMail;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|AlertResult[]
     */
    public function getAlertResults(): Collection
    {
        return $this->alertResults;
    }

    public function addAlertResult(AlertResult $alertResult): self
    {
        if (!$this->alertResults->contains($alertResult)) {
            $this->alertResults[] = $alertResult;
            $alertResult->setUserAlert($this);
        }

        return $this;
    }

    public function removeAlertResult(AlertResult $alertResult): self
    {
        if ($this->alertResults->removeElement($alertResult)) {
            // set the owning side to null (unless already changed)
            if ($alertResult->getUserAlert() === $this) {
                $alertResult->setUserAlert(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
