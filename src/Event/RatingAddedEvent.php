<?php

namespace App\Event;

use App\Entity\Deal;
use App\Entity\User;

class RatingAddedEvent extends UserEvent
{
    private $deal;

    public function __construct(User $user, Deal $deal)
    {
        $this->deal = $deal;

        parent::__construct($user);
    }

    public function getDeal(): Deal
    {
        return $this->deal;
    }
}
