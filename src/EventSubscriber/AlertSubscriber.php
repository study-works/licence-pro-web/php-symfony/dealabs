<?php

namespace App\EventSubscriber;

use App\Entity\AlertResult;
use App\Entity\Deal;
use App\Entity\UserAlert;
use App\Event\DealPublishedEvent;
use App\Event\DealSeenEvent;
use App\Event\RatingAddedEvent;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AlertSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            DealPublishedEvent::class => 'onDealPublished',
            RatingAddedEvent::class   => 'onRatingAdded',
            DealSeenEvent::class      => 'onDealSeen',
        ];
    }

    public function onDealPublished(DealPublishedEvent $event)
    {
        $this->checkAlert($event->getDeal());
    }

    public function onRatingAdded(RatingAddedEvent $event)
    {
        $this->checkAlert($event->getDeal());
    }

    public function onDealSeen(DealSeenEvent $event)
    {
        $user = $event->getUser();
        if (!isset($user)) {
            return;
        }

        $alertResults = $this->entityManager->getRepository(AlertResult::class)
            ->getDealResultOfUser($user->getId(), $event->getDeal()->getId());

        foreach ($alertResults as $alertResult) {
            $alertResult->setIsSee(true);
        }

        $this->entityManager->flush();
    }

    private function checkAlert(Deal $deal): void
    {
        $userAlerts = $this->entityManager->getRepository(UserAlert::class)->findAll();

        foreach ($userAlerts as $userAlert) {
            if ($userAlert->getMinRating() > 0) { // The deal has just been posted, so it must have a rating of 0
                continue;
            }

            if (
                false !== stristr($deal->getTitle(), $userAlert->getKeyword()) ||
                false !== stristr($deal->getDescription(), $userAlert->getKeyword())
            ) {
                $this->createAlertResult($userAlert, $deal);
                continue;
            }

            if (null !== $deal->getSeller() && false !== stristr($deal->getSeller()->getName(), $userAlert->getKeyword())) {
                $this->createAlertResult($userAlert, $deal);
                continue;
            }

            foreach ($deal->getGroups() as $group) {
                if (false !== stristr($group->getName(), $userAlert->getKeyword())) {
                    $this->createAlertResult($userAlert, $deal);
                }
            }
        }
    }

    private function createAlertResult(UserAlert $userAlert, Deal $deal)
    {
        try {
            $alertResult = new AlertResult();
            $alertResult->setUserAlert($userAlert);
            $alertResult->setDeal($deal);

            $this->entityManager->persist($alertResult);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $ucve) {
            // Do Nothing,
            // The alert result already exist
            // It's OK.
        }
    }
}
