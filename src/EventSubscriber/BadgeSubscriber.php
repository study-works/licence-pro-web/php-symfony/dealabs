<?php

namespace App\EventSubscriber;

use App\Entity\Badge;
use App\Entity\User;
use App\Entity\UserBadge;
use App\Event\CommentAddedEvent;
use App\Event\DealPublishedEvent;
use App\Event\RatingAddedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BadgeSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RatingAddedEvent::class   => 'onRatingAdded',
            DealPublishedEvent::class => 'onDealPublished',
            CommentAddedEvent::class  => 'onCommentAdded',
        ];
    }

    public function onRatingAdded(RatingAddedEvent $event)
    {
        $this->updateUserBadge($event->getUser(), 'Surveillant');
    }

    public function onDealPublished(DealPublishedEvent $event)
    {
        $this->updateUserBadge($event->getUser(), 'Cobaye');
    }

    public function onCommentAdded(CommentAddedEvent $event)
    {
        $this->updateUserBadge($event->getUser(), 'Rapport de stage');
    }

    private function updateUserBadge(User $user, string $badgeName): void
    {
        /** @var Badge $badge */
        $badge = $this->entityManager->getRepository(Badge::class)->findOneBy(['name' => $badgeName]);

        /** @var UserBadge $userBadge */
        $userBadge = $this->entityManager->getRepository(UserBadge::class)->findOneBy(['user' => $user->getId(), 'badge' => $badge->getId()]);

        $value = $userBadge->getValue() + 1;
        $userBadge->setValue($value);
        if ($value >= $badge->getValue()) {
            $userBadge->setIsWin(true);
        }

        try {
            $this->entityManager->flush();
        } catch (Exception $e) {
            // Do nothing, It's for when the exception "The EntityManager is closed." is thrown.
            // But it's thrown when an another flush is did, so it's ok
        }
    }
}
