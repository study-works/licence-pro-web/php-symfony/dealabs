<?php

namespace App\Form\Type;

use App\Entity\UserAlert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('keyword', TextType::class, [
                'attr' => [
                    'placeholder' => 'user.alerts.create.keyword',
                ],
                'label'    => 'user.alerts.create.keyword',
            ])
            ->add('minRating', NumberType::class, [
                'attr' => [
                    'placeholder' => 'user.alerts.create.min_rating',
                ],
                'label'    => 'user.alerts.create.min_rating',
            ])
            ->add('sendMail', CheckboxType::class, [
                'label'       => 'user.alerts.create.send_email',
                'required'    => false,
            ])
            ->add('send', SubmitType::class, [
                'label' => 'user.alerts.create.send',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserAlert::class,
        ]);
    }
}
