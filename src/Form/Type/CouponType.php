<?php

namespace App\Form\Type;

use App\Entity\Coupon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CouponType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('deal', DealType::class, [
                'label' => false,
            ])
            ->add('type', ChoiceType::class, [
                'attr' => [
                    'class' => 'js-deal-input-type',
                ],
                'choices'  => [
                    'rate'          => 0,
                    'price'         => 1,
                    'free_delivery' => 2,
                ],
                'choice_label' => function ($choice, $key, $value) {
                    switch ($choice) {
                        case 0:
                            return 'coupon.create.type.rate';
                        case 1:
                            return 'coupon.create.type.price';
                        case 2:
                            return 'coupon.create.type.free_delivery';
                    }
                },
                'label'       => 'coupon.create.type',
                'placeholder' => 'coupon.create.type.choose',
                'required'    => false,
            ])
            ->add('reduction', NumberType::class, [
                'attr' => [
                    'placeholder' => 'coupon.create.reduction.placeholder',
                    'class'       => 'js-deal-input-reduction',
                ],
                'help'     => 'coupon.create.reduction.help',
                'label'    => 'coupon.create.reduction',
                'required' => false,
            ])
            ->add('send', SubmitType::class, [
                'label' => 'coupon.create.send',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Coupon::class,
        ]);
    }
}
