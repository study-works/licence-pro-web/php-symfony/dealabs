<?php

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo', TextType::class, [
                    'attr' => [
                        'placeholder' => 'user.pseudo',
                    ],
                    'help'     => 'user.pseudo.length',
                    'label'    => 'user.pseudo',
                ])
            ->add('email', EmailType::class, [
                    'attr' => [
                        'placeholder' => 'user.email',
                    ],
                    'label'    => 'user.email',
                ])
            ->add('password', PasswordType::class, [
                    'attr' => [
                        'placeholder' => 'signin.password',
                    ],
                    'help'       => 'signin.password.length',
                    'label'      => 'signin.password',
                ])
            ->add('send', SubmitType::class, [
                    'label' => 'signin.send',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
