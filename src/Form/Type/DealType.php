<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('link', UrlType::class, [
                'attr' => [
                    'placeholder' => 'deal.create.link.placeholder',
                ],
                'help'     => 'deal.create.link.help',
                'label'    => 'deal.create.link',
                'required' => false,
            ])
            ->add('coupon', TextType::class, [
                'attr' => [
                    'placeholder' => 'deal.create.coupon.placeholder',
                    'class'       => 'js-deal-input-coupon',
                ],
                'label'    => 'deal.create.coupon',
                'required' => false,
            ])
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => 'deal.create.title.placeholder',
                    'class'       => 'js-deal-input-title',
                ],
                'label'    => 'deal.create.title',
            ])
            ->add('description', TextareaType::class, [
                'attr'  => [
                    'class' => 'js-deal-input-description',
                ],
                'label' => 'deal.create.description',
            ])
            ->add('imageFile', VichImageType::class, [
                'attr' => [
                    'class' => 'js-deal-input-img',
                ],
                'label'    => 'deal.create.image',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
        ]);
    }
}
