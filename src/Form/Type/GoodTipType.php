<?php

namespace App\Form\Type;

use App\Entity\GoodTip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoodTipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('deal', DealType::class, [
                'label' => false,
            ])
            ->add('price', NumberType::class, [
                'attr' => [
                    'placeholder' => 'good_type.create.price.placeholder',
                    'class'       => 'js-deal-input-price',
                ],
                'label'    => 'good_type.create.price',
                'required' => false,
            ])
            ->add('usual_price', NumberType::class, [
                'attr' => [
                    'placeholder' => 'good_type.create.usual_price.placeholder',
                    'class'       => 'js-deal-input-usual-price',
                ],
                'label'    => 'good_type.create.usual_price',
                'required' => false,
            ])
            ->add('delivery_costs', NumberType::class, [
                'attr' => [
                    'placeholder' => 'good_type.create.delivery_costs.placeholder',
                    'class'       => 'js-deal-input-delivery-costs',
                ],
                'label'    => 'good_type.create.delivery_costs',
                'required' => false,
            ])
            ->add('send', SubmitType::class, [
                'label' => 'good_type.create.send',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GoodTip::class,
        ]);
    }
}
