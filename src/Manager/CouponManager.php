<?php

namespace App\Manager;

use App\Entity\Coupon;
use Doctrine\ORM\EntityManagerInterface;

class CouponManager extends DealManager
{
    public const TYPE_RATE          = 0;
    public const TYPE_PRICE         = 1;
    public const TYPE_FREE_DELIVERY = 2;

    private $couponRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->couponRepository = $entityManager->getRepository(Coupon::class);
    }

    /**
     * Retrieve all coupons with users.
     */
    public function getAllWithDependencies(): array
    {
        $deals = $this->couponRepository->getAllWithDependencies();

        return parent::getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Retrieves all Hot coupons with users.
     */
    public function getAllHotWithDependencies(): array
    {
        $deals = $this->couponRepository->getAllHotWithDependencies();

        return parent::getDealsWithNbCommentsAndRatings($deals);
    }
}
