<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Deal;
use App\Entity\DealRating;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class DealManager
{
    private $dealRepository;
    private $commentRepository;
    private $ratingRepository;
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->dealRepository    = $entityManager->getRepository(Deal::class);
        $this->commentRepository = $entityManager->getRepository(Comment::class);
        $this->ratingRepository  = $entityManager->getRepository(DealRating::class);
        $this->userRepository    = $entityManager->getRepository(User::class);
    }

    /**
     * Retrieves the deals of the week sorted by the number of comments.
     * It also returns the number of comments and ratings.
     */
    public function getLastWeekByNbComment(): array
    {
        $deals = $this->dealRepository->getLastByNbComment('-1 week');

        return $this->getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Retrieves the hottest deals.
     * It also returns the number of comments and ratings.
     */
    public function getAllHotByDate(): array
    {
        $deals = $this->dealRepository->getAllHotByDate();

        return $this->getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Recovers the hottest deals of the day.
     */
    public function getLastDayByRating(): array
    {
        return $this->dealRepository->getLastByRating('-1 day');
    }

    /**
     * Retrieve a deal by its ID.
     */
    public function getDealById(int $id): Deal
    {
        return $this->dealRepository->find($id);
    }

    /**
     * Retrieve a deal by its Slug.
     */
    public function getDealBySlug(string $slug): Deal
    {
        return $this->dealRepository->findOneBy(['slug' => $slug]);
    }

    /**
     * Recover a complete deal through its slug.
     *
     * @throws NonUniqueResultException
     */
    public function findDealBySlug(string $slug, ?int $userID): ?array
    {
        $deal = $this->dealRepository->findOneFullBy(['slug' => $slug]);

        if (isset($userID)) {
            $deal['user'] = $this->userRepository->getSavedDeal($userID, $deal[0]->getId());
        }

        return $deal;
    }

    /**
     * Search a deal.
     */
    public function searchDeal(string $value): ?array
    {
        $deals = $this->dealRepository->searchDeal($value);

        if (empty($deals[0])) {
            return null;
        }

        return $this->getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Retrieve all deals of a User.
     */
    public function getUserDeals(int $userId): ?array
    {
        $deals = $this->dealRepository->getuserDeals($userId);

        if (empty($deals[0])) {
            return null;
        }

        return $this->getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Get all deals from an alert.
     */
    public function getDealsFromAlert(int $userAlertId, bool $onlyNotSeen = false, bool $onlyToday = false): ?array
    {
        $deals = $this->dealRepository->getDealsFromAlert($userAlertId, $onlyNotSeen, $onlyToday);

        if (empty($deals[0])) {
            return null;
        }

        return $this->getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Retrieves comment numbers and scores from a list of deals.
     */
    protected function getDealsWithNbCommentsAndRatings(array $deals): array
    {
        $dealsId = array_column($deals, 'id');
        $deals   = array_column($deals, '0');

        $nbComments = $this->commentRepository->getNbCommentsForDealList($dealsId);
        $nbComments = array_column($nbComments, 'nbComments', 'id');

        $ratings = $this->ratingRepository->getRatingForDealList($dealsId);
        $ratings = array_column($ratings, 'rating', 'id');

        return [$deals, $nbComments, $ratings];
    }
}
