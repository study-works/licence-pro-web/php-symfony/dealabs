<?php

namespace App\Manager;

use App\Entity\GoodTip;
use Doctrine\ORM\EntityManagerInterface;

class GoodTipManager extends DealManager
{
    private $goodTipRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->goodTipRepository = $entityManager->getRepository(GoodTip::class);
    }

    /**
     * Recover all the good tips with the users.
     */
    public function getAllWithDependencies(): array
    {
        $deals = $this->goodTipRepository->getAllWithDependencies();

        return parent::getDealsWithNbCommentsAndRatings($deals);
    }

    /**
     * Get all the Hot tips with users.
     */
    public function getAllHotWithDependencies(): array
    {
        $deals = $this->goodTipRepository->getAllHotWithDependencies();

        return parent::getDealsWithNbCommentsAndRatings($deals);
    }
}
