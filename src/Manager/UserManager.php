<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Deal;
use App\Entity\DealRating;
use App\Entity\User;
use App\Entity\UserBadge;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    private $commentRepository;
    private $dealRepository;
    private $ratingRepository;
    private $userBadge;
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->commentRepository = $entityManager->getRepository(Comment::class);
        $this->dealRepository    = $entityManager->getRepository(Deal::class);
        $this->ratingRepository  = $entityManager->getRepository(DealRating::class);
        $this->userBadge         = $entityManager->getRepository(UserBadge::class);
        $this->userRepository    = $entityManager->getRepository(User::class);
    }

    public function getSavedDeals(int $userId): ?array
    {
        $user = $this->userRepository->getSavedDeals($userId);

        if (!isset($user[0])) {
            return null;
        }

        return $this->getDealsWithNbCommentsAndRatings($user);
    }

    /**
     * Get statistics of a User.
     */
    public function getUserStatistics(int $userId): array
    {
        $statistics = [];

        $statistics = $statistics + $this->dealRepository->getNbUserDeals($userId);
        $statistics = $statistics + $this->commentRepository->getNbUserDeals($userId);

        $biggerRating = $this->dealRepository->getBiggerRatingUserDeals($userId);
        if (isset($biggerRating)) {
            $statistics = $statistics + $biggerRating;
        } else {
            $statistics['biggerRating'] = 0;
        }

        $ratings           = array_column($this->dealRepository->getAvgRatingUserDealsOneYear($userId), 'rating');
        $ratingsSize       = count($ratings);
        $statistics['avg'] = 0 != $ratingsSize ? array_sum($ratings) / count($ratings) : 0;

        $nbHotDeals            = count($this->dealRepository->getUserDealsHot($userId));
        $statistics['rateHot'] = 0 != $statistics['nbDeals'] ? $nbHotDeals / $statistics['nbDeals'] : 0;

        return $statistics;
    }

    /**
     * Get all badges of a User.
     */
    public function getFullUserBadge(int $userId): array
    {
        return $this->userBadge->getFullUserBadge($userId);
    }

    /**
     * Retrieves comment numbers and scores from a list of deals.
     */
    protected function getDealsWithNbCommentsAndRatings(array $user): array
    {
        $deals = $user[0]->getSavedDeals()->map(function ($obj) {
            return $obj;
        })->toArray();

        $dealsId = [];

        foreach ($deals as $deal) {
            $dealsId[] = $deal->getId();
        }

        $nbComments = $this->commentRepository->getNbCommentsForDealList($dealsId);
        $nbComments = array_column($nbComments, 'nbComments', 'id');

        $ratings = $this->ratingRepository->getRatingForDealList($dealsId);
        $ratings = array_column($ratings, 'rating', 'id');

        return [$deals, $nbComments, $ratings];
    }
}
