<?php

namespace App\Repository;

use App\Entity\AlertResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AlertResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlertResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlertResult[]    findAll()
 * @method AlertResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlertResult::class);
    }

    /**
     * Retrieves all result for a deal and a user.
     *
     * @param int $userId The user ID
     * @param int $dealId The deal ID
     */
    public function getDealResultOfUser(int $userId, int $dealId)
    {
        return $this->createQueryBuilder('ar')
            ->join('ar.userAlert', 'ua')
            ->andWhere('ar.deal = :dealId')
            ->setParameter('dealId', $dealId)
            ->andWhere('ua.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return AlertResult[] Returns an array of AlertResult objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlertResult
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
