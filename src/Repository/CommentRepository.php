<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * Retrieves the number of comment deals for a list of IDs.
     */
    public function getNbCommentsForDealList(array $ids)
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c) AS nbComments')
            ->leftJoin('c.deal', 'd')
            ->addSelect('d.id')
            ->where('d.id in (:ids)')
            ->setParameter('ids', $ids)
            ->groupBy('c.deal')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Get the number of comments of a USer.
     *
     * @throws NonUniqueResultException
     */
    public function getNbUserDeals(int $userId): array
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c) AS nbComments')
            ->where('c.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
