<?php

namespace App\Repository;

use App\Entity\Coupon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @method Coupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coupon[]    findAll()
 * @method Coupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouponRepository extends ServiceEntityRepository
{
    private $parameterBag;

    private $hotRating;

    public function __construct(ManagerRegistry $registry, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;

        $this->hotRating = $this->parameterBag->get('hot_rating');

        parent::__construct($registry, Coupon::class);
    }

    /**
     * Retrieve all coupons with users.
     */
    public function getAllWithDependencies()
    {
        return $this->createQueryBuilder('c')
            ->addSelect('c.id')
            ->leftJoin('c.user', 'u')
            ->addSelect('u')
            ->andWhere('c.isExpired = 0')
            ->addOrderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Retrieves all Hot coupons with users.
     */
    public function getAllHotWithDependencies()
    {
        return $this->createQueryBuilder('c')
            ->addSelect('c.id')
            ->leftJoin('c.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS HIDDEN rating')
            ->leftJoin('c.user', 'u')
            ->addSelect('u')
            ->andWhere('c.isExpired = 0')
            ->andHaving('rating > :minRating')
            ->setParameter('minRating', $this->hotRating)
            ->addOrderBy('c.createdAt', 'DESC')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Coupon[] Returns an array of Coupon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Coupon
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
