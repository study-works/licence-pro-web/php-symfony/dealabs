<?php

namespace App\Repository;

use App\Entity\DealRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DealRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method DealRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method DealRating[]    findAll()
 * @method DealRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DealRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DealRating::class);
    }

    /**
     * Retrieves deal notes for a list of IDs.
     */
    public function getRatingForDealList(array $ids)
    {
        return $this->createQueryBuilder('dr')
            ->select('SUM(dr.value) AS rating')
            ->leftJoin('dr.deal', 'd')
            ->addSelect('d.id')
            ->where('d.id in (:ids)')
            ->setParameter('ids', $ids)
            ->groupBy('dr.deal')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return DealRating[] Returns an array of DealRating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DealRating
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
