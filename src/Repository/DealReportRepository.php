<?php

namespace App\Repository;

use App\Entity\DealReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DealReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method DealReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method DealReport[]    findAll()
 * @method DealReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DealReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DealReport::class);
    }

    // /**
    //  * @return DealReport[] Returns an array of DealReport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DealReport
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
