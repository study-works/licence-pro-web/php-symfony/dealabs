<?php

namespace App\Repository;

use App\Entity\Deal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @method Deal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Deal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Deal[]    findAll()
 * @method Deal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DealRepository extends ServiceEntityRepository
{
    private $parameterBag;

    private $hotRating;

    public function __construct(ManagerRegistry $registry, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;

        $this->hotRating = $this->parameterBag->get('hot_rating');

        parent::__construct($registry, Deal::class);
    }

    /**
     * Retrieves the deals of the week sorted by the number of comments.
     */
    public function getLastByNbComment(string $time)
    {
        return $this->createQueryBuilder('d')
            ->addSelect('d.id')
            ->andWhere('d.createdAt > :date')
            ->setParameter('date', date('Y-m-d', strtotime($time)))
            ->leftJoin('d.comments', 'c')
            ->addSelect('COUNT(c) AS HIDDEN nbComments')
            ->leftJoin('d.user', 'u')
            ->addSelect('u')
            ->andWhere('d.isExpired = 0')
            ->addOrderBy('nbComments', 'DESC')
            ->addOrderBy('d.createdAt', 'DESC')
            ->addGroupBy('d.id')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Retrieves the hottest deals.
     */
    public function getAllHotByDate()
    {
        return $this->createQueryBuilder('d')
            ->addSelect('d.id')
            ->leftJoin('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS HIDDEN rating')
            ->leftJoin('d.user', 'u')
            ->addSelect('u')
            ->andWhere('d.isExpired = 0')
            ->andHaving('rating > :minRating')
            ->setParameter('minRating', $this->hotRating)
            ->addOrderBy('d.createdAt', 'DESC')
            ->addGroupBy('d')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Recovers the hottest deals of the day.
     */
    public function getLastByRating(string $time)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.createdAt > :date')
            ->setParameter('date', date('Y-m-d', strtotime($time)))
            ->leftJoin('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS rating')
            ->andWhere('d.isExpired = 0')
            ->andHaving('rating > :minRating')
            ->setParameter('minRating', $this->hotRating)
            ->addOrderBy('rating', 'DESC')
            ->groupBy('d')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneFullBy(array $criteria): ?array
    {
        $qb = $this->createQueryBuilder('d')
            ->join('d.user', 'u')
            ->addSelect('u')
            ->leftJoin('d.comments', 'c')
            ->addSelect('c')
            ->leftJoin('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) as rating')
            ->addGroupBy('c.id')
            ->leftJoin('c.user', 'cu')
            ->addSelect('cu')
            ->leftJoin('d.groups', 'g')
            ->addSelect('g')
            ->addGroupBy('g.id')
            ->leftJoin('d.seller', 's')
            ->addSelect('s')
        ;

        foreach ($criteria as $key => $where) {
            $qb->andWhere('d.' . $key . ' = :where')
                ->setParameter('where', $where)
            ;
        }

        return $qb->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Search deal.
     */
    public function searchDeal(string $value): array
    {
        return $this->createQueryBuilder('d')
            ->addSelect('d.id')
            ->leftJoin('d.user', 'u')
            ->addSelect('u')
            ->leftJoin('d.groups', 'g')
            ->addSelect('g')
            ->leftJoin('d.seller', 's')
            ->addSelect('s')
            ->andWhere('d.title LIKE :value')
            ->andWhere('d.isExpired = 0')
            ->orWhere('d.description LIKE :value')
            ->orWhere('g.name LIKE :value')
            ->orWhere('s.name LIKE :value')
            ->setParameter(':value', '%' . $value . '%')
            ->addOrderBy('d.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Retrieves all deals of a User.
     */
    public function getuserDeals(int $userId): array
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.user = :userId')
            ->setParameter('userId', $userId)
            ->addSelect('d.id')
            ->leftJoin('d.user', 'u')
            ->addSelect('u')
            ->andWhere('d.isExpired = 0')
            ->addOrderBy('d.createdAt', 'DESC')
            ->addGroupBy('d.id')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Get the number of deals of a USer.
     *
     * @throws NonUniqueResultException
     */
    public function getNbUserDeals(int $userId): array
    {
        return $this->createQueryBuilder('d')
            ->select('COUNT(d) AS nbDeals')
            ->where('d.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get the warmer deal of a USer.
     *
     * @throws NonUniqueResultException
     */
    public function getBiggerRatingUserDeals(int $userId): ?array
    {
        return $this->createQueryBuilder('d')
            ->join('d.dealRatings', 'dr')
            ->select('SUM(dr.value) AS biggerRating')
            ->where('d.user = :userId')
            ->setParameter('userId', $userId)
            ->addGroupBy('d.id')
            ->orderBy('biggerRating', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * Get all ratings of deals User.
     */
    public function getAvgRatingUserDealsOneYear(int $userId): array
    {
        return $this->createQueryBuilder('d')
            ->join('d.dealRatings', 'dr')
            ->select('SUM(dr.value) AS rating')
            ->where('d.user = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('d.createdAt > :date')
            ->setParameter('date', date('Y-m-d', strtotime('-1 year')))
            ->addGroupBy('d.id')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get all hot deals of a User.
     */
    public function getUserDealsHot(int $userId): array
    {
        return $this->createQueryBuilder('d')
            ->select('d.id') // The ID is not necessary, but it allows not to have the complete deals.
            ->join('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS HIDDEN rating')
            ->where('d.user = :userId')
            ->setParameter('userId', $userId)
            ->andHaving('rating > :minRating')
            ->setParameter('minRating', $this->hotRating)
            ->addGroupBy('d.id')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get all deals from an alert.
     */
    public function getDealsFromAlert(int $userAlertId, bool $onlyNotSeen = false, bool $onlyToday = false): array
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('d.id')
            ->join('d.alertResults', 'ar')
            ->andWhere('ar.userAlert = :userAlertId')
            ->setParameter('userAlertId', $userAlertId)
            ->leftJoin('d.user', 'u')
            ->addSelect('u')
            ->andWhere('d.isExpired = 0')
            ->addOrderBy('d.createdAt', 'DESC')
            ->addGroupBy('d.id')
        ;

        if ($onlyNotSeen) {
            $qb->andWhere('ar.isSee = 0');
        }

        if ($onlyToday) {
            $qb->andWhere('d.createdAt > :date')
                ->setParameter('date', date('Y-m-d', strtotime('-1 day')));
        }

        return $qb->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get last deals.
     * Used for the API.
     */
    public function getLast(string $time)
    {
        return $this->createQueryBuilder('d')
            ->leftJoin('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS rating')
            ->leftJoin('d.groups', 'g')
            ->addSelect('g')
            ->leftJoin('d.seller', 's')
            ->addSelect('s')
            ->andWhere('d.createdAt > :date')
            ->setParameter('date', date('Y-m-d', strtotime($time)))
            ->andWhere('d.isExpired = 0')
            ->addOrderBy('d.createdAt', 'DESC')
            ->groupBy('g')
            ->addGroupBy('d')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get user saved deals.
     * Used for the API.
     */
    public function getUserSavedDeals(int $userId)
    {
        return $this->createQueryBuilder('d')
            ->join('d.usersWhoSaved', 'uws')
            ->where('uws.id = :userId')
            ->setParameter('userId', $userId)
            ->leftJoin('d.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS rating')
            ->leftJoin('d.groups', 'g')
            ->addSelect('g')
            ->leftJoin('d.seller', 's')
            ->addSelect('s')
            ->andWhere('d.isExpired = 0')
            ->addOrderBy('d.createdAt', 'DESC')
            ->groupBy('g')
            ->addGroupBy('d')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Deal[] Returns an array of Deal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Deal
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
