<?php

namespace App\Repository;

use App\Entity\GoodTip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @method GoodTip|null find($id, $lockMode = null, $lockVersion = null)
 * @method GoodTip|null findOneBy(array $criteria, array $orderBy = null)
 * @method GoodTip[]    findAll()
 * @method GoodTip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GoodTipRepository extends ServiceEntityRepository
{
    private $parameterBag;

    private $hotRating;

    public function __construct(ManagerRegistry $registry, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;

        $this->hotRating = $this->parameterBag->get('hot_rating');

        parent::__construct($registry, GoodTip::class);
    }

    /**
     * Recover all the good tips with the users.
     */
    public function getAllWithDependencies()
    {
        return $this->createQueryBuilder('gt')
            ->addSelect('gt.id')
            ->leftJoin('gt.user', 'u')
            ->addSelect('u')
            ->andWhere('gt.isExpired = 0')
            ->addOrderBy('gt.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Get all the Hot tips with users.
     */
    public function getAllHotWithDependencies()
    {
        return $this->createQueryBuilder('gt')
            ->addSelect('gt.id')
            ->leftJoin('gt.dealRatings', 'dr')
            ->addSelect('SUM(dr.value) AS HIDDEN rating')
            ->leftJoin('gt.user', 'u')
            ->addSelect('u')
            ->andWhere('gt.isExpired = 0')
            ->andHaving('rating > :minRating')
            ->setParameter('minRating', $this->hotRating)
            ->addOrderBy('gt.createdAt', 'DESC')
            ->groupBy('gt.id')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return GoodTip[] Returns an array of GoodTip objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GoodTip
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
