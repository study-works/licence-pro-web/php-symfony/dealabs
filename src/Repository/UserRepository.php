<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.pseudo = :username')
            ->orWhere('u.email = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * Retrieves all users belonging to a certain role.
     */
    public function getByRole(string $role): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%' . $role . '%')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Retrieves a deal saved by a user.
     *
     * @throws NonUniqueResultException
     */
    public function getSavedDeal(int $userId, int $dealId)
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.savedDeals', 'sd')
            ->andWhere('u.id = :userId')
            ->andWhere('sd.id = :dealId')
            ->setParameter('userId', $userId)
            ->setParameter('dealId', $dealId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * Retrieves all deals saved by a user.
     *
     * @throws NonUniqueResultException
     */
    public function getSavedDeals(int $userId): ?array
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.savedDeals', 'sd')
            ->addSelect('sd')
            ->leftJoin('sd.user', 'us')
            ->addSelect('us')
            ->addSelect('sd.id')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('sd.createdAt', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
